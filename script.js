const jsonFile = process.argv.slice(2)

const puppeteer = require('puppeteer');
const fs = require('fs');
const readline = require('readline');

let rawdata = fs.readFileSync(jsonFile[0]);
let json = JSON.parse(rawdata);


function askQuestion(query) {
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
    });

    return new Promise(resolve => rl.question(query, ans => {
        rl.close();
        resolve(ans);
    }))
}
function daysDifference(added){
    d = added.split(' ')[0]
    d = d.split('.')
    d1 = new Date(d[0], parseInt(d[1]) - 1, d[2])
    d2 = new Date()
    let dt = Math.abs(d2.getTime() - d1.getTime());
    let days = Math.ceil(dt / (1000 * 60 * 60 * 24));
    // console.log(days);
    return days;
}

async function getPic() {

    const ws = fs.createWriteStream("out.csv");
    ans = []
    datas = []
    url = json['url']
    next_page = json['next_page_query']

    links_selector = json['links_selector']
    data_field = json['data_field']
    selector = data_field['selector']
    regex = data_field['regex']
    scraping_content_query = data_field['scraping_content_query']

    stopCount = json['limit']['stopCount']
    stopDay = json['limit']['stopDay']

    breaking_flag = 0

    const browser = await puppeteer.launch({
        headless: true,
        // args: ['--start-fullscreen']
    });
    const page = await browser.newPage();
    await page.goto(url);
    console.log('start  ')


    per_page_link_count = await page.evaluate("document.querySelectorAll('"+links_selector+"').length");        
    
    for (var p = 0; p < stopCount; p++) {
        link_list = await page.evaluate((links_selector) => {
            let out = []
            x=document.querySelectorAll( links_selector )
            x.forEach( (x)=>{ out.push( x.querySelector('a').href ) } )
            return out;
        },links_selector);

        console.log(link_list)
        ans = link_list

        console.log('page - ', p + 1)

        const page2 = await browser.newPage();

        console.log('second-loop')
        for (i = 0; i < per_page_link_count; i++) { 
            await page2.goto(ans[i])
            data={}
            scraping_content = await page2.evaluate(scraping_content_query);

            for(x in selector){
                data[x]=await page2.evaluate( selector[x] )
            }
            for(x in regex){
                d=scraping_content.match( regex[x] )
                t=''
                if (d!=null) t=d[1]
                data[x]=t 
            }

            console.log(data)

            // daysDifference()
            datas.push( data )

        } //2nd-loop
        await page2.close()

        if (breaking_flag == 1) break
        ////next-page////
        await Promise.all([
            page.evaluate(next_page),
            page.waitForNavigation({ waitUntil: 'networkidle0' })
        ]);



    } //for
    
    var data = JSON.stringify(datas);
    fs.writeFile("output."+jsonFile, data, (err) => {
      if (err) console.log(err);
    });
    console.log('done')
    await page.close();
    await browser.close();

}
getPic();